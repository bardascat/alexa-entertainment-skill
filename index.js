// Lambda Function code for Alexa.
// Paste this into your index.js file. 

const Alexa = require("ask-sdk");
const axios = require('axios')
const eventApiPath = 'http://oreo-s-crib.go.ro/event';
const streamerApiPath = 'http://oreo-s-crib.go.ro/streamer';




const invocationName = "my home";

// Session Attributes 
//   Alexa will track attributes for you, by default only during the lifespan of your session.
//   The history[] array will track previous request(s), used for contextual Help/Yes/No handling.
//   Set up DynamoDB persistence to have the skill save and reload these attributes between skill sessions.

function getMemoryAttributes() {   const memoryAttributes = {
       "history":[],

        // The remaining attributes will be useful after DynamoDB persistence is configured
       "launchCount":0,
       "lastUseTimestamp":0,

       "lastSpeechOutput":{},
       "nextIntent":[]

       // "favoriteColor":"",
       // "name":"",
       // "namePronounce":"",
       // "email":"",
       // "mobileNumber":"",
       // "city":"",
       // "state":"",
       // "postcode":"",
       // "birthday":"",
       // "bookmark":0,
       // "wishlist":[],
   };
   return memoryAttributes;
};

const maxHistorySize = 20; // remember only latest 20 intents 


// 1. Intent Handlers =============================================

const AMAZON_NavigateHomeIntent_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'AMAZON.NavigateHomeIntent' ;
    },
    handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;
        let sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

        let say = 'Hello from AMAZON.NavigateHomeIntent. ';


        return responseBuilder
            .speak(say)
            .reprompt('try again, ' + say)
            .getResponse();
    },
};

const AMAZON_CancelIntent_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'AMAZON.CancelIntent' ;
    },
    handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;
        let sessionAttributes = handlerInput.attributesManager.getSessionAttributes();


        let say = 'Okay, talk to you later! ';

        return responseBuilder
            .speak(say)
            .withShouldEndSession(true)
            .getResponse();
    },
};

const AMAZON_HelpIntent_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'AMAZON.HelpIntent' ;
    },
    handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;
        let sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

        let intents = getCustomIntents();
        let sampleIntent = randomElement(intents);

        let say = 'You asked for help. '; 

        // let previousIntent = getPreviousIntent(sessionAttributes);
        // if (previousIntent && !handlerInput.requestEnvelope.session.new) {
        //     say += 'Your last intent was ' + previousIntent + '. ';
        // }
        // say +=  'I understand  ' + intents.length + ' intents, '

        say += ' Here something you can ask me, ' + getSampleUtterance(sampleIntent);

        return responseBuilder
            .speak(say)
            .reprompt('try again, ' + say)
            .getResponse();
    },
};

const AMAZON_StopIntent_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'AMAZON.StopIntent' ;
    },
    handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;
        let sessionAttributes = handlerInput.attributesManager.getSessionAttributes();


        let say = 'Okay, talk to you later! ';

        return responseBuilder
            .speak(say)
            .withShouldEndSession(true)
            .getResponse();
    },
};

const RemoteControls_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'RemoteControls' ;
    },
    async handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;

        let say = '';

        let slotValues = getSlotValues(request.intent.slots);

        console.log('***** slotValues: ' +  JSON.stringify(slotValues, null, 2));


        if( (slotValues.deviceCommand.ERstatus === 'ER_SUCCESS_NO_MATCH') ||  (!slotValues.deviceCommand.heardAs) ||
            (slotValues.device.ERstatus === 'ER_SUCCESS_NO_MATCH') ||  (!slotValues.device.heardAs) ) {
            say = 'Cannot understand command. Try again !'
        } else {

            //happy path
            const payloads = [];
            if (slotValues.device.heardAs=='speakers'){
                payloads.push({"remote":'atol', "action": mapDeviceAction('atol', slotValues.deviceCommand.heardAs)});
                payloads.push({"remote":'topping', "action": mapDeviceAction('topping', slotValues.deviceCommand.heardAs)});
            } else {
                let remote = mapDeviceName(slotValues.device.heardAs);
                payloads.push({"remote":remote, "action": mapDeviceAction(remote, slotValues.deviceCommand.heardAs)});
            }

            console.log("starting to send request - payloads" + JSON.stringify(payloads));

            for (const p of payloads) {
                console.log("Going to send payload: " + JSON.stringify(p));

                await axios
                    .post(eventApiPath, p)
                    .then(res => { console.log(`Received response from endpoint: statusCode: ${res.status}`) })
                    .catch(error => {
                        say = 'Error communicating with server.';
                        console.log(error);
                    })

            }

            console.log("done sending request");
            //say = 'Done setting '+slotValues.device.heardAs + ' to ' + slotValues.deviceCommand.heardAs
        }

        return responseBuilder
            .speak(say)
            .getResponse();
    },
};

const SetVolume_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'SetVolume' ;
    },
    async handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;

        let say = '';

        let slotValues = getSlotValues(request.intent.slots);
        console.log('***** slotValues: ' +  JSON.stringify(slotValues, null, 2));

        if( (slotValues.value.ERstatus === 'ER_SUCCESS_NO_MATCH') ||  (!slotValues.value.heardAs) ) {
            say = 'Cannot understand command. Try again !'
        } else {

            //say = 'Setting speakers to '+slotValues.value.heardAs + ' volume';
            const p = {"remote":'atol', "action": 'ATOL_SET_VOLUME','value': slotValues.value.heardAs};

            console.log("starting to send request - payload" + JSON.stringify(p));

            await axios
                .post(eventApiPath, p)
                .then(res => { console.log(`Received response from endpoint: statusCode: ${res.status}`) })
                .catch(error => {
                    say = 'Error communicating with server.';
                    console.log(error);
                })
        }

        return responseBuilder
            .speak(say)
            .getResponse();
    },
};

const PlayPlaylist_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'PlayPlaylist' ;
    },
    async handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;

        let say = '';
        let slotValues = getSlotValues(request.intent.slots);

        console.log('***** slotValues: ' +  JSON.stringify(slotValues, null, 2));
        
        if( (slotValues.playlist.ERstatus === 'ER_SUCCESS_NO_MATCH') ||  (!slotValues.playlist.heardAs) ) {
            say = 'Cannot understand playlist name. Try again !'
        } else {
            say = 'Playing '+slotValues.playlist.heardAs + ' playlist on speakers';

            if(slotValues.startType.heardAs == "cold"){
                toggleOnOffSpeakers(slotValues.startType);
            }
            
            const p = {"action": 'PLAY_PLAYLIST','value': slotValues.playlist.heardAs};
            console.log("starting to send request - payload" + JSON.stringify(p));

            await axios
                .post(streamerApiPath, p)
                .then(res => { console.log(`Received response from endpoint: statusCode: ${res.status}`) })
                .catch(error => {
                    say = 'Error communicating with server.';
                    console.log(error);
                })
        
        }

        return responseBuilder
            .speak(say)
            .getResponse();
    },
};

const PlayArtist_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'PlayArtist' ;
    },
    async handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;
        let say = '';

        let slotValues = getSlotValues(request.intent.slots);
        console.log('***** slotValues: ' +  JSON.stringify(slotValues, null, 2));

        if( (slotValues.name.ERstatus === 'ER_SUCCESS_NO_MATCH') ||  (!slotValues.name.heardAs) ) {
            say = 'Cannot understand artist name. Try again !'
        } else {
            say = 'Playing '+slotValues.name.heardAs + ' on speakers';

            if(slotValues.startType.heardAs == "cold"){
                toggleOnOffSpeakers(slotValues.startType);
            }
            const p = {"action": 'PLAY_ARTIST','value': slotValues.name.heardAs};
            
            console.log("starting to send request - payload" + JSON.stringify(p));
            await axios
                .post(streamerApiPath, p)
                .then(res => { console.log(`Received response from endpoint: statusCode: ${res.status}`) })
                .catch(error => {
                    say = 'Error communicating with server.';
                    console.log(error);
                })
        }

        return responseBuilder
            .speak(say)
            .getResponse();
    },
};

const bored_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'bored' ;
    },
    handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;


        const responses = [
            "You should cum on Nico's tities",
            "You can play with the pussycat",
            "You can fuck Nico nice and slow !",
            "Go and ask Nico to play with your cock",
        ];

        return responseBuilder
            .speak(responses[Math.floor(Math.random() * 4)])
            .getResponse();
    },
};


const nextTrack_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'nextTrack' ;
    },
    async handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;
        let sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

        say = 'Playing next track';

        const p = { "action": 'PLAY_NEXT','value': null };
        
        console.log("starting to send request - payload" + JSON.stringify(p));
        await axios
            .post(streamerApiPath, p)
            .then(res => { console.log(`Received response from endpoint: statusCode: ${res.status}`) })
            .catch(error => {
                say = 'Error communicating with server.';
                console.log(error);
            })
    

        return responseBuilder
            .speak(say)
            .getResponse();
    },
};

const previousTrack_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'previousTrack' ;
    },
    async handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;
        say = 'Playing previous track';

        const p = { "action": 'PLAY_PREVIOUS','value': null };
        
        console.log("starting to send request - payload" + JSON.stringify(p));
        await axios
            .post(streamerApiPath, p)
            .then(res => { console.log(`Received response from endpoint: statusCode: ${res.status}`) })
            .catch(error => {
                say = 'Error communicating with server.';
                console.log(error);
            })
    

        return responseBuilder
            .speak(say)
            .getResponse();
    },
};

const PauseMusic_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'PauseMusic' ;
    },
    async handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;

        say = 'Music Paused';

        const p = { "action": 'PAUSE_MUSIC','value': null };
        
        console.log("starting to send request - payload" + JSON.stringify(p));
        await axios
            .post(streamerApiPath, p)
            .then(res => { console.log(`Received response from endpoint: statusCode: ${res.status}`) })
            .catch(error => {
                say = 'Error communicating with server.';
                console.log(error);
            })
    

        return responseBuilder
            .speak(say)
            .getResponse();
    },
};

const UnpauseMusic_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'UnpauseMusic' ;
    },
    async handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;
        
        say = 'Music resumed';

        const p = { "action": 'PLAY_MUSIC','value': null };
        
        console.log("starting to send request - payload" + JSON.stringify(p));
        await axios
            .post(streamerApiPath, p)
            .then(res => { console.log(`Received response from endpoint: statusCode: ${res.status}`) })
            .catch(error => {
                say = 'Error communicating with server.';
                console.log(error);
            })
    

        return responseBuilder
            .speak(say)
            .getResponse();
    },
};

const LaunchRequest_Handler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'LaunchRequest';
    },
    handle(handlerInput) {
        const responseBuilder = handlerInput.responseBuilder;

        let say = 'hello' + ' and welcome to ' + invocationName + ' ! Say help to hear some options.';

        let skillTitle = capitalize(invocationName);


        return responseBuilder
            .speak(say)
            .reprompt('try again, ' + say)
            .withStandardCard('Welcome!', 
              'Hello!\nThis is a card for your skill, ' + skillTitle,
               welcomeCardImg.smallImageUrl, welcomeCardImg.largeImageUrl)
            .getResponse();
    },
};

const SessionEndedHandler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);
        return handlerInput.responseBuilder.getResponse();
    }
};

const ErrorHandler =  {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        const request = handlerInput.requestEnvelope.request;

        console.log(`Error handled: ${error.message}`);
        // console.log(`Original Request was: ${JSON.stringify(request, null, 2)}`);

        return handlerInput.responseBuilder
            .speak('Sorry, an error occurred.  Please say again.')
            .reprompt('Sorry, an error occurred.  Please say again.')
            .getResponse();
    }
};


// 2. Constants ===========================================================================

    // Here you can define static data, to be used elsewhere in your code.  For example: 
    //    const myString = "Hello World";
    //    const myArray  = [ "orange", "grape", "strawberry" ];
    //    const myObject = { "city": "Boston",  "state":"Massachusetts" };

const APP_ID = undefined;  // TODO replace with your Skill ID (OPTIONAL).

// 3.  Helper Functions ===================================================================

function capitalize(myString) {

     return myString.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); }) ;
}

 
function randomElement(myArray) { 
    return(myArray[Math.floor(Math.random() * myArray.length)]); 
} 
 
function stripSpeak(str) { 
    return(str.replace('<speak>', '').replace('</speak>', '')); 
} 
 
 
 
 
function getSlotValues(filledSlots) { 
    const slotValues = {}; 
 
    Object.keys(filledSlots).forEach((item) => { 
        const name  = filledSlots[item].name; 
 
        if (filledSlots[item] && 
            filledSlots[item].resolutions && 
            filledSlots[item].resolutions.resolutionsPerAuthority[0] && 
            filledSlots[item].resolutions.resolutionsPerAuthority[0].status && 
            filledSlots[item].resolutions.resolutionsPerAuthority[0].status.code) { 
            switch (filledSlots[item].resolutions.resolutionsPerAuthority[0].status.code) { 
                case 'ER_SUCCESS_MATCH': 
                    slotValues[name] = { 
                        heardAs: filledSlots[item].value, 
                        resolved: filledSlots[item].resolutions.resolutionsPerAuthority[0].values[0].value.name, 
                        ERstatus: 'ER_SUCCESS_MATCH' 
                    }; 
                    break; 
                case 'ER_SUCCESS_NO_MATCH': 
                    slotValues[name] = { 
                        heardAs: filledSlots[item].value, 
                        resolved: '', 
                        ERstatus: 'ER_SUCCESS_NO_MATCH' 
                    }; 
                    break; 
                default: 
                    break; 
            } 
        } else { 
            slotValues[name] = { 
                heardAs: filledSlots[item].value, 
                resolved: '', 
                ERstatus: '' 
            }; 
        } 
    }, this); 
 
    return slotValues; 
} 
 
function getExampleSlotValues(intentName, slotName) { 
 
    let examples = []; 
    let slotType = ''; 
    let slotValuesFull = []; 
 
    let intents = model.interactionModel.languageModel.intents; 
    for (let i = 0; i < intents.length; i++) { 
        if (intents[i].name == intentName) { 
            let slots = intents[i].slots; 
            for (let j = 0; j < slots.length; j++) { 
                if (slots[j].name === slotName) { 
                    slotType = slots[j].type; 
 
                } 
            } 
        } 
         
    } 
    let types = model.interactionModel.languageModel.types; 
    for (let i = 0; i < types.length; i++) { 
        if (types[i].name === slotType) { 
            slotValuesFull = types[i].values; 
        } 
    } 
 
 
    examples.push(slotValuesFull[0].name.value); 
    examples.push(slotValuesFull[1].name.value); 
    if (slotValuesFull.length > 2) { 
        examples.push(slotValuesFull[2].name.value); 
    } 
 
 
    return examples; 
} 
 
function sayArray(myData, penultimateWord = 'and') { 
    let result = ''; 
 
    myData.forEach(function(element, index, arr) { 
 
        if (index === 0) { 
            result = element; 
        } else if (index === myData.length - 1) { 
            result += ` ${penultimateWord} ${element}`; 
        } else { 
            result += `, ${element}`; 
        } 
    }); 
    return result; 
} 
function supportsDisplay(handlerInput) // returns true if the skill is running on a device with a display (Echo Show, Echo Spot, etc.) 
{                                      //  Enable your skill for display as shown here: https://alexa.design/enabledisplay 
    const hasDisplay = 
        handlerInput.requestEnvelope.context && 
        handlerInput.requestEnvelope.context.System && 
        handlerInput.requestEnvelope.context.System.device && 
        handlerInput.requestEnvelope.context.System.device.supportedInterfaces && 
        handlerInput.requestEnvelope.context.System.device.supportedInterfaces.Display; 
 
    return hasDisplay; 
} 
 
 
const welcomeCardImg = { 
    smallImageUrl: "https://s3.amazonaws.com/skill-images-789/cards/card_plane720_480.png", 
    largeImageUrl: "https://s3.amazonaws.com/skill-images-789/cards/card_plane1200_800.png" 
 
 
}; 
 
const DisplayImg1 = { 
    title: 'Jet Plane', 
    url: 'https://s3.amazonaws.com/skill-images-789/display/plane340_340.png' 
}; 
const DisplayImg2 = { 
    title: 'Starry Sky', 
    url: 'https://s3.amazonaws.com/skill-images-789/display/background1024_600.png' 
 
}; 
 
function getCustomIntents() { 
    const modelIntents = model.interactionModel.languageModel.intents; 
 
    let customIntents = []; 
 
 
    for (let i = 0; i < modelIntents.length; i++) { 
 
        if(modelIntents[i].name.substring(0,7) != "AMAZON." && modelIntents[i].name !== "LaunchRequest" ) { 
            customIntents.push(modelIntents[i]); 
        } 
    } 
    return customIntents; 
} 
 
function getSampleUtterance(intent) { 
 
    return randomElement(intent.samples); 
 
} 
 
function getPreviousIntent(attrs) { 
 
    if (attrs.history && attrs.history.length > 1) { 
        return attrs.history[attrs.history.length - 2].IntentRequest; 
 
    } else { 
        return false; 
    } 
 
} 
 
function getPreviousSpeechOutput(attrs) { 
 
    if (attrs.lastSpeechOutput && attrs.history.length > 1) { 
        return attrs.lastSpeechOutput; 
 
    } else { 
        return false; 
    } 
 
} 
 
function timeDelta(t1, t2) { 
 
    const dt1 = new Date(t1); 
    const dt2 = new Date(t2); 
    const timeSpanMS = dt2.getTime() - dt1.getTime(); 
    const span = { 
        "timeSpanMIN": Math.floor(timeSpanMS / (1000 * 60 )), 
        "timeSpanHR": Math.floor(timeSpanMS / (1000 * 60 * 60)), 
        "timeSpanDAY": Math.floor(timeSpanMS / (1000 * 60 * 60 * 24)), 
        "timeSpanDesc" : "" 
    }; 
 
 
    if (span.timeSpanHR < 2) { 
        span.timeSpanDesc = span.timeSpanMIN + " minutes"; 
    } else if (span.timeSpanDAY < 2) { 
        span.timeSpanDesc = span.timeSpanHR + " hours"; 
    } else { 
        span.timeSpanDesc = span.timeSpanDAY + " days"; 
    } 
 
 
    return span; 
 
} 
 
 
const InitMemoryAttributesInterceptor = { 
    process(handlerInput) { 
        let sessionAttributes = {}; 
        if(handlerInput.requestEnvelope.session['new']) { 
 
            sessionAttributes = handlerInput.attributesManager.getSessionAttributes(); 
 
            let memoryAttributes = getMemoryAttributes(); 
 
            if(Object.keys(sessionAttributes).length === 0) { 
 
                Object.keys(memoryAttributes).forEach(function(key) {  // initialize all attributes from global list 
 
                    sessionAttributes[key] = memoryAttributes[key]; 
 
                }); 
 
            } 
            handlerInput.attributesManager.setSessionAttributes(sessionAttributes); 
 
 
        } 
    } 
}; 
 
const RequestHistoryInterceptor = { 
    process(handlerInput) { 
 
        const thisRequest = handlerInput.requestEnvelope.request; 
        let sessionAttributes = handlerInput.attributesManager.getSessionAttributes(); 
 
        let history = sessionAttributes['history'] || []; 
 
        let IntentRequest = {}; 
        if (thisRequest.type === 'IntentRequest' ) { 
 
            let slots = []; 
 
            IntentRequest = { 
                'IntentRequest' : thisRequest.intent.name 
            }; 
 
            if (thisRequest.intent.slots) { 
 
                for (let slot in thisRequest.intent.slots) { 
                    let slotObj = {}; 
                    slotObj[slot] = thisRequest.intent.slots[slot].value; 
                    slots.push(slotObj); 
                } 
 
                IntentRequest = { 
                    'IntentRequest' : thisRequest.intent.name, 
                    'slots' : slots 
                }; 
 
            } 
 
        } else { 
            IntentRequest = {'IntentRequest' : thisRequest.type}; 
        } 
        if(history.length > maxHistorySize - 1) { 
            history.shift(); 
        } 
        history.push(IntentRequest); 
 
        handlerInput.attributesManager.setSessionAttributes(sessionAttributes); 
 
    } 
 
}; 
 
 
 
 
const RequestPersistenceInterceptor = { 
    process(handlerInput) { 
 
        if(handlerInput.requestEnvelope.session['new']) { 
 
            return new Promise((resolve, reject) => { 
 
                handlerInput.attributesManager.getPersistentAttributes() 
 
                    .then((sessionAttributes) => { 
                        sessionAttributes = sessionAttributes || {}; 
 
 
                        sessionAttributes['launchCount'] += 1; 
 
                        handlerInput.attributesManager.setSessionAttributes(sessionAttributes); 
 
                        handlerInput.attributesManager.savePersistentAttributes() 
                            .then(() => { 
                                resolve(); 
                            }) 
                            .catch((err) => { 
                                reject(err); 
                            }); 
                    }); 
 
            }); 
 
        } // end session['new'] 
    } 
}; 
 
 
const ResponseRecordSpeechOutputInterceptor = { 
    process(handlerInput, responseOutput) { 
 
        let sessionAttributes = handlerInput.attributesManager.getSessionAttributes(); 
        let lastSpeechOutput = { 
            "outputSpeech":responseOutput.outputSpeech.ssml, 
            "reprompt":responseOutput.reprompt.outputSpeech.ssml 
        }; 
 
        sessionAttributes['lastSpeechOutput'] = lastSpeechOutput; 
 
        handlerInput.attributesManager.setSessionAttributes(sessionAttributes); 
 
    } 
}; 
 
const ResponsePersistenceInterceptor = { 
    process(handlerInput, responseOutput) { 
 
        const ses = (typeof responseOutput.shouldEndSession == "undefined" ? true : responseOutput.shouldEndSession); 
 
        if(ses || handlerInput.requestEnvelope.request.type == 'SessionEndedRequest') { // skill was stopped or timed out 
 
            let sessionAttributes = handlerInput.attributesManager.getSessionAttributes(); 
 
            sessionAttributes['lastUseTimestamp'] = new Date(handlerInput.requestEnvelope.request.timestamp).getTime(); 
 
            handlerInput.attributesManager.setPersistentAttributes(sessionAttributes); 
 
            return new Promise((resolve, reject) => { 
                handlerInput.attributesManager.savePersistentAttributes() 
                    .then(() => { 
                        resolve(); 
                    }) 
                    .catch((err) => { 
                        reject(err); 
                    }); 
 
            }); 
 
        } 
 
    } 
}; 
 
 
 
// 4. Exports handler function and setup ===================================================
const skillBuilder = Alexa.SkillBuilders.standard();
exports.handler = skillBuilder
    .addRequestHandlers(
        AMAZON_NavigateHomeIntent_Handler, 
        AMAZON_CancelIntent_Handler, 
        AMAZON_HelpIntent_Handler, 
        AMAZON_StopIntent_Handler, 
        RemoteControls_Handler, 
        SetVolume_Handler, 
        PlayPlaylist_Handler, 
        PlayArtist_Handler, 
        bored_Handler, 
        nextTrack_Handler, 
        previousTrack_Handler, 
        PauseMusic_Handler, 
        UnpauseMusic_Handler, 
        LaunchRequest_Handler, 
        SessionEndedHandler
    )
    .addErrorHandlers(ErrorHandler)
    .addRequestInterceptors(InitMemoryAttributesInterceptor)
    .addRequestInterceptors(RequestHistoryInterceptor)

   // .addResponseInterceptors(ResponseRecordSpeechOutputInterceptor)

 // .addRequestInterceptors(RequestPersistenceInterceptor)
 // .addResponseInterceptors(ResponsePersistenceInterceptor)

 // .withTableName("askMemorySkillTable")
 // .withAutoCreateTable(true)

    .lambda();


// End of Skill code -------------------------------------------------------------
// Static Language Model for reference

const model = {
  "interactionModel": {
    "languageModel": {
      "invocationName": "my home",
      "intents": [
        {
          "name": "AMAZON.NavigateHomeIntent",
          "samples": []
        },
        {
          "name": "AMAZON.CancelIntent",
          "samples": []
        },
        {
          "name": "AMAZON.HelpIntent",
          "samples": []
        },
        {
          "name": "AMAZON.StopIntent",
          "samples": []
        },
        {
          "name": "RemoteControls",
          "slots": [
            {
              "name": "device",
              "type": "deviceType"
            },
            {
              "name": "deviceCommand",
              "type": "deviceCommand"
            }
          ],
          "samples": [
            "{deviceCommand} {device}",
            "{device} {deviceCommand}",
            "Turn {deviceCommand} the {device}"
          ]
        },
        {
          "name": "SetVolume",
          "slots": [
            {
              "name": "value",
              "type": "AMAZON.NUMBER",
              "multipleValues": {
                "enabled": true
              }
            }
          ],
          "samples": [
            "volume to {value} on speakers",
            "set volume to {value} on speakers"
          ]
        },
        {
          "name": "PlayPlaylist",
          "slots": [
            {
              "name": "playlist",
              "type": "playlists"
            },
            {
              "name": "startType",
              "type": "startType"
            }
          ],
          "samples": [
            "Play {playlist} playlist on {startType} start",
            "{playlist}  playlist on {startType} start"
          ]
        },
        {
          "name": "PlayArtist",
          "slots": [
            {
              "name": "name",
              "type": "AMAZON.Artist"
            },
            {
              "name": "startType",
              "type": "startType"
            }
          ],
          "samples": [
            "{name}  artist  on {startType} start",
            "play {name}  artist on {startType} start"
          ]
        },
        {
          "name": "bored",
          "slots": [],
          "samples": [
            "I am bored",
            "I am bored what should I do"
          ]
        },
        {
          "name": "nextTrack",
          "slots": [],
          "samples": [
            "play next track",
            "next",
            "next track on speakers",
            "next track"
          ]
        },
        {
          "name": "previousTrack",
          "slots": [],
          "samples": [
            "previous track"
          ]
        },
        {
          "name": "PauseMusic",
          "slots": [],
          "samples": [
            "stop music",
            "pause track",
            "pause music"
          ]
        },
        {
          "name": "UnpauseMusic",
          "slots": [],
          "samples": [
            "play music",
            "resume music",
            "start music",
            "unpause music"
          ]
        },
        {
          "name": "LaunchRequest"
        }
      ],
      "types": [
        {
          "name": "deviceType",
          "values": [
            {
              "name": {
                "value": "T.V"
              }
            },
            {
              "name": {
                "value": "tv"
              }
            },
            {
              "name": {
                "value": "speakers"
              }
            },
            {
              "name": {
                "value": "amplifier"
              }
            },
            {
              "name": {
                "value": "duck"
              }
            }
          ]
        },
        {
          "name": "deviceCommand",
          "values": [
            {
              "name": {
                "value": "right"
              }
            },
            {
              "name": {
                "value": "left"
              }
            },
            {
              "name": {
                "value": "mute"
              }
            },
            {
              "name": {
                "value": "ambilight"
              }
            },
            {
              "name": {
                "value": "volume down"
              }
            },
            {
              "name": {
                "value": "volume up"
              }
            },
            {
              "name": {
                "value": "off"
              }
            },
            {
              "name": {
                "value": "on"
              }
            }
          ]
        },
        {
          "name": "volumeCommand",
          "values": [
            {
              "name": {
                "value": "decrease"
              }
            },
            {
              "name": {
                "value": "increase"
              }
            }
          ]
        },
        {
          "name": "mediaType",
          "values": [
            {
              "name": {
                "value": "artist"
              }
            },
            {
              "name": {
                "value": "song"
              }
            },
            {
              "name": {
                "value": "playlist"
              }
            }
          ]
        },
        {
          "name": "playlists",
          "values": [
            {
              "name": {
                "value": "techno"
              }
            },
            {
              "name": {
                "value": "jazz"
              }
            },
            {
              "name": {
                "value": "night"
              }
            },
            {
              "name": {
                "value": "morning"
              }
            }
          ]
        },
        {
          "name": "startType",
          "values": [
            {
              "name": {
                "value": "warm"
              }
            },
            {
              "name": {
                "value": "cold"
              }
            }
          ]
        }
      ]
    }
  }
};


const mapDeviceName = function (deviceName) {

    switch (deviceName) {

        case 'amplifier':{
            return 'atol'
        }break;

        case 'duck':{
            return 'topping';
        }
        default: {
            return deviceName.toLowerCase();
        }
    }
}

const mapDeviceAction = function (deviceName, deviceAction) {

    switch (deviceName) {
        case 'atol':{
            return mapAtolActions(deviceAction);
        }break;
        case 'topping':{
            return mapToppingActions(deviceAction);
        }
        case 'tv':{
            return mapTvActions(deviceAction);
        }
    }
}

const mapAtolActions = function (action) {

    switch (action) {

        case "on":{
            return "ATOL_POWER_ON_OFF";
        }break;
        case "off":{
            return "ATOL_POWER_ON_OFF";
        }break;
        case "volume up":{
            return "ATOL_VOLUME_UP_3";
        }break;
        case "volume down":{
            return "ATOL_VOLUME_DOWN_3";
        }break;
        case "mute":{
            return "ATOL_SELECT_MUTE";
        }break;
        case "unmute":{
            return "ATOL_SELECT_MUTE";
        }break;
        default: {
            return null;
        }
    }
}

const mapToppingActions = function (action) {

    switch (action) {

        case "on" : {
            return 'TOPPING_POWER_ON_OFF';
        } break;
        case "off" : {
            return 'TOPPING_POWER_ON_OFF';
        } break;
        case "left" : {
            return 'TOPPING_INPUT_SELECT_LEFT';
        } break;
        case "right" : {
            return 'TOPPING_INPUT_SELECT_RIGHT';
        } break;
        default: {
            return null;
        }

    }
}


const mapTvActions = function (action) {

    switch (action) {

        case "on" : {
            return 'TV_POWER_ON_OFF';
        } break;
        case "off" : {
            return 'TV_POWER_ON_OFF';
        } break;
        case "mute" : {
            return 'TV_MUTE';
        } break;
        default: {
            console.log("Could not map tv action:" + action)
            return null;
        }

    }
}

const toggleOnOffSpeakers = async function (startType){

    const payloads=[];
    
    payloads.push({"remote": 'atol','action': "ATOL_POWER_ON_OFF"});
    payloads.push({"remote": 'topping','action': "TOPPING_POWER_ON_OFF"});

    for (const p of payloads) {

        console.log("starting to send request - payload" + JSON.stringify(p));

        await axios
            .post(eventApiPath, p)
            .then(res => { console.log(`Received response from endpoint: statusCode: ${res.status}`) })
            .catch(error => {
                say = 'Error communicating with server.';
                console.log(error);
            })
    }

}